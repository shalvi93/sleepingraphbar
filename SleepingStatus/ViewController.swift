//
//  ViewController.swift
//  SleepingStatus
//
//  Created by Sierra 4 on 06/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    
    @IBOutlet weak var viewBar: UIView!
    var frameView : CGFloat = 0
    var tagArray = [Int]()
    var uiViewArray = [UIView]()

    override func viewDidLoad() {
        super.viewDidLoad()
        
        addBarInView(hour : 1, minute :10,color: UIColor.orange , tag : 1)
        addBarInView(hour: 1, minute: 12, color: UIColor.orange, tag : 1)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        addBarInView(hour: 1, minute: 10, color: UIColor.blue, tag : 3)
        addBarInView(hour: 1, minute: 25, color: UIColor.darkGray, tag : 1)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        addBarInView(hour: 1, minute: 12, color: UIColor.orange, tag : 1)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        addBarInView(hour: 1, minute: 12, color: UIColor.orange, tag : 1)
        addBarInView(hour: 1, minute: 12, color: UIColor.orange, tag : 1)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        addBarInView(hour: 1, minute: 12, color: UIColor.black, tag : 2)
        
       }
   
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        testTouches(touches: touches as NSSet!)
    }
    
    
    func addBarInView(hour : Int , minute : Int , color : UIColor , tag : Int){
    
    let gestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(getter: viewBar))
    
    let minuteduration = Float(viewBar.frame.width / 1456)//1440
    
    var length = Float()
        length = minuteduration * Float(minute + hour * 60)
    
    let view:UIView = UIView.init(frame: CGRect.init(x: 0, y: 0, width: CGFloat(length), height: viewBar.frame.height))
        uiViewArray.append(view)
        view.frame.origin.x = frameView
        frameView = frameView + CGFloat(length)
        view.backgroundColor = color
        viewBar.addSubview(view)
        view.addGestureRecognizer(gestureRecognizer)
    
       frameView = frameView + CGFloat(length)
       viewBar.addSubview(view)
       tagArray.append(tag)
       print("view:",uiViewArray)
    }
    
    
    func testTouches(touches: NSSet!) {
       
        let touch = touches.allObjects[0] as! UITouch
        let touchLocation = touch.location(in:touch.view)
        let touchloc = touch.previousLocation(in: viewBar)
        for obstacleView in uiViewArray
        {
            let obstacleViewFrame = self.view.convert(obstacleView.frame, from: obstacleView.superview)
            
            if obstacleViewFrame.contains(touchLocation)
            {
                obstacleView.backgroundColor = UIColor.black
                print("gg")
            }
            else if obstacleViewFrame.contains(touchloc)
       {
                obstacleView.backgroundColor = UIColor.blue
            }
//            else
//            {
//               obstacleView.backgroundColor = UIColor.white
//            }
    }
}
}

