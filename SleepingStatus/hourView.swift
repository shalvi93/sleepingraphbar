//
//  hourView.swift
//  SleepingStatus
//
//  Created by Sierra 4 on 07/04/17.
//  Copyright © 2017 Sierra 4. All rights reserved.
//

import Foundation
class hourView: UIView{
    
    var dataToRepresent:[[String:Any]] = []
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    func clearChart() {
        dataToRepresent.removeAll()
    }
    
    override func draw(_ rect: CGRect) {
        guard let currentGraphicsContext = UIGraphicsGetCurrentContext() else { return }
        var progressRect = rect
        var color1 = UIColor(red: 149.0/255.0, green: 159.0/255.0, blue: 169.0/255.0, alpha: 1.0)
        var color2 = UIColor(red: 240.0/255.0, green: 73.0/255.0, blue: 37.0/255.0, alpha: 1.0)
        let percentage : CGFloat = 0.1
        progressRect.size.width *= percentage
        progressRect.origin.x = 0.20
        currentGraphicsContext.addRect(progressRect)
        currentGraphicsContext.setFillColor(color2.cgColor)
        currentGraphicsContext.fill(progressRect)
        
    }
}

}

    
    
    
    
    
    
    
    
}
